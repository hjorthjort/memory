//Rikard Hjort & Kristoffer Knutsson
//Labbgrupp 14

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
class Back implements Icon {
	private int width, height;
	private Color color;

	public Back (int width, int height, Color color) {
		this.width = width;
		this.height = height;
		this.color = color;
	}

	public int getIconHeight() {
		return height;
	}

	public int getIconWidth() {
		return width;
	}

	public void paintIcon (Component c, Graphics g, int x, int y) {
		g.setColor(color);
		g.fillRect(0,0, c.getWidth(), c.getHeight());
	}
}*/

public class Kort extends JColorfulButton {
//	private static Icon back = new Back(0,0, Color.BLUE);
	private static Color back = Color.BLUE;
	private static Color rolloverBack = new Color(0, 0, 210);
	private final Icon ICON;
	private Status status;

	//Enum for cards' status
	public enum Status {
		SAKNAS, DOLT, SYNLIGT
	}

	/*
	Constructors
	*/

	//Takes one parameter Icon icon
	public Kort (Icon icon) {
		super();
		this.ICON = icon;
		this.setOpaque(true);
		this.setStatus(Status.SAKNAS);
		this.setVisibility();
	}

	//Takes two parameters: Icon icon and Status status (enum type)
	public Kort (Icon icon, Status status) {
		super();
		this.ICON = icon;
		this.status = status;
		this.setOpaque(true);
		this.setStatus(status);
		this.setVisibility();
	}

	//Changes visibility depending on status. Should be called whenever status is set or changes. 
	private void setVisibility() {
		if (this.status == Status.SYNLIGT) {
			//this.setVisible(true);
			this.setIcon(ICON);
		} else if (this.status == Status.DOLT) {
			this.setIcon(null);
			this.setBackground(back);
		} else if (this.status == Status.SAKNAS) {
			this.setIcon(null);
			this.setBackground(Color.WHITE);
		}
	}

	//Change cards status. Calls on setVisibility function. 
	public void setStatus(Status status) {
		this.status = status;
		this.setVisibility();
	}

	public Icon getMyIconConstant () {
		return this.ICON;
	}

	//Get cards status
	public Status getStatus() {
		return this.status;
	}

	//Make a copy of this instance of Kort
	public Kort copy() {
		return new Kort(this.ICON, this.status);
	}

	//Test if cards are equal in regards of icon and status.
	public boolean sammaBild(Kort compareKort) {
		if (compareKort.getMyIconConstant() == this.ICON) {
			return true;
		} 
		else {
			return false;
		}
	}

	public void setRegularBackground() {
		setBackground(back);
	}

	public void setRolloverBackground() {
		setBackground(rolloverBack);
	}

}
