//Rikard Hjort & Kristoffer Knutsson
//Labbgrupp 14

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import counter.*;
import java.util.Arrays;
//import javax.sound.sampled.*;
import javax.swing.BorderFactory;
import ratnum.RatNum;


//Creates a player object. This also defines methods for manipulation score and focus.
class Player extends JPanel {
	private final Color FOCUS_COLOR = Color.YELLOW;
	private final Color UNFOCUS_COLOR = Color.GRAY;
	private final Font NAME_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
	private final Font SCORE_FONT = new Font(Font.SANS_SERIF, Font.BOLD, 20);

	private boolean focused; //if the player has the turn, this is set to true.
	private String name;
	private JLabel nameLabel;
	private FastCounter score; //keeps track of the player's score.
	private JLabel scoreLabel;

	//CONSTRUCTORS
	
	/*
	* Creates a new player.
	* The name is set to "Player X" where x is the number of the player. If player 
	* is number 1, she is set to be in focus. Else she will be passive.
	* The initial score for all players is set to 0.
	*/
	public Player (int nmbr) {
		super(new GridLayout(2,1));
		if (nmbr == 1) {
			setFocus(true);
		} else {
			setFocus(false);
		}
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		name = "Player " + nmbr;
		nameLabel = new JLabel(name, SwingConstants.CENTER);
		nameLabel.setFont(NAME_FONT);
		add(nameLabel);
		
		score = new FastCounter((int)1e9, 2);	//sets the score counter to increment by 2 
												//for every successful play. Counter's roof is set
												//so high that it can never be reached under 
												//reasonable circumstances.
		update(score.getValue());
	} // end of constructor

	//QUERIES
	
	/*
	* Query the player for her score.
	*/
	public int getScore () {
		return score.getValue();
	}

	//COMMANDS 

	/*
	Updates the Player-panel with new score. 
	Should be called whenever score is changed (or initialized).
	*/
	public void update (int newScore) {
		if (scoreLabel != null) { //Checks that there is a score label to remove.
			remove(scoreLabel);
		}
		scoreLabel = new JLabel(Integer.toString(newScore), SwingConstants.CENTER);
		scoreLabel.setFont(SCORE_FONT);
		add(scoreLabel);
	}

	public void redefineCounter(int modulo, int step) {
		score = new FastCounter(modulo, step);
	}

	/*
	* Makes the player either focused (has turn) or unfocused (passive).
	* Having the turn is, as a standard, indicated by a yellow background, being passive 
	* by a gray background.
	*/
	public void setFocus(boolean bool) {
		focused = bool;
		if (focused == true) {
			this.setBackground(FOCUS_COLOR);
		} else if (focused == false) {
			this.setBackground(UNFOCUS_COLOR);
		}
	}

	/*
	*Increases player's score by predefined number of points: as a standard, 2 points 
	* for multiplayer game, 1 point for single player.
	*/
	public void incScore () {
		score.upMany();
		update(score.getValue());
	}

	/*
	* Player score is set to 0.
	*/
	public void resetScore () {
		score.reset();
		update(score.getValue());
	}
} // end of class Player

/* 
* A special type of player, specialized to play single player games
*/
class SoloPlayer extends Player {
	private int lastScore;

	public SoloPlayer () {
		super(1);
		redefineCounter((int)1e9, 1); 	//changes score counter to increment by 1 
										//for every successful play.
	}
} //end of class SoloPlayer

/*
* A specialized panel for single player games.
* It prints out the number of guesses for previous rounds, up to the last 10 rounds played.
*/
class SoloCounter extends JPanel {
	private int[] scores = new int[10];
	private int rounds = 0;

	public SoloCounter () {
		super(new GridLayout(10,1));
	}

	/*
	* Prints out the scores from the last 10 previous rounds.
	*/
	public void addScore (int newScore) {
		for (int i = scores.length - 1; 0 < i; i--) {
			scores[i] = scores[i-1];
		}
		scores[0] = newScore;
		removeAll();
		rounds++;
		for (int i = 0; i < scores.length; i++) {
			if (scores[i] == 0) {
				break;	//if less than 10 rounds have been played, this ensures that nothing 
						//unnecesary is printed.
			}
			JLabel tempLabel = new JLabel("Omgång " + (rounds-i) + ": " + scores[i]);
			add(tempLabel);
		}
		validate();	//update this panel.
	}
} // end of class SoloCounter

/*
* The panel in which the game is played.
* This panel is responsible for holding and flipping cards, and for reporting to the Memory-game if
* a successful or failed guess has been made.
*/
class CardsPanel extends JPanel implements ActionListener, MouseListener { 
	private Memory parent;
	private Timer timer;
	private int waitTime = 1500; 	// the initial delay of the timer is set to this 
									//value. May be changed in 
									//menu Inställningar>>Ändra fördröjning
	private Kort[] cards; //all available cards. number determined by available images.
	private boolean delay;	//makes cards unclickable when true.
	private int turnCount; //to keep track if one or two cards have been turned
	private Kort firstCard;	//the first card flipped up.
	private Kort secondCard; //the second --------||-----
	private boolean success; //is set to true only if the player flipped two of the same.
	private int cardsLeft;	//keeps track of how many cards are left. When zero, 
							//method gameOver is called.

	//CONSTRUCTORS

	public CardsPanel(int rows, int cols, int gridSize, Memory parent) {
		super(new GridLayout(rows, cols));
		setPreferredSize(new Dimension(cols*gridSize, rows*gridSize));
		timer = new Timer(waitTime, this);
		delay = false;
		turnCount = 0;
		this.parent = parent;
		File bildmapp = new File("bildmapp"); 					//Create an array for all cards
		File[] images = bildmapp.listFiles();					// ------||-------
		if (images[0].getName().equals(".DS_Store")) { 			// ------||-------
			images = Arrays.copyOfRange(images, 1, images.length);// ------||-------
		}														// ------||-------				
		cards = new Kort[images.length];						// ------||-------
		for ( int i = 0; i < cards.length; i++) {				// ------||-------
			cards[i] = new Kort(new ImageIcon(images[i].getPath()));// ------||-------
		}//end creating cards-array
	} // end of constructor

	private void gameChecker(Kort kort) {
		if (turnCount == 0) {
			firstCard = kort;
			turnCount++;
		} else if (turnCount == 1) {
			secondCard = kort;
			turnCount = 0;
			delay = true;
			if (firstCard.sammaBild(kort)) {
				success = true;
			} else {
				success = false;
			}
			timer.start();
		}
	}

	//QUERIES

	public int getNbrOfCards () {
		return cards.length;
	}

	int getTurnCount() {
		return turnCount;
	}

	void resetTurnCount() {
		turnCount = 0;
	}

	//COMMANDS

	/*
	* Turn card over and reveal it's picture
	*/
	private void flipUp (Kort card) {
		card.setStatus(Kort.Status.SYNLIGT);
	}

	/*
	* Turn card over and hide it's picture
	*/
	private void flipDown (Kort card) {
		card.setStatus(Kort.Status.DOLT);
	}

	/*
	* Remove card from the game.
	*/
	private void removeCard (Kort card) {
		card.setStatus(Kort.Status.SAKNAS);
		cardsLeft--;
	}

	/*
	* Successful guess! Removes the chosen cards and calls on the successfulPlay method in 
	*Memory-instance. If these were the last cards in the game, the gameOver method in memory 
	* is called directly after.
	*/
	private void success() {
		removeCard(firstCard);
		removeCard(secondCard);
		parent.successfulPlay();
		if (cardsLeft == 0) {
			parent.gameOver();
		}
	}

	/*
	* Failed play. Cards are turned back over and the failedPlay method in Memory-instance 
	* is called.
	*/
	private void failure() {
		flipDown(firstCard);
		flipDown(secondCard);
		parent.failedPlay();
	}

	/*
	* This method opens a dialog window where a user may input a new number of seconds to hold 
	* chosen cards open befor play is resumed. Allowed formats for input is integer numbers, 
	* floating points number with decimal part indicated by either '.' or ',', and rational 
	* numbers on the form a/b.
	*/
	public void changeTimer() {
		String nbrString = JOptionPane.showInputDialog(this, "Välj en ny vänttid (sekunder):", 
												"Memory", JOptionPane.QUESTION_MESSAGE);
		nbrString = nbrString.replace(',', '.'); //For more differnet ways of keying in number

		double nbr = -1;
		try {
			try {
				RatNum rat = RatNum.parse(nbrString);
				nbr = rat.toDouble();
			} catch (NumberFormatException n) {
				nbr = Double.parseDouble(nbrString);
			}
			if (nbr < 0) {
				JOptionPane.showMessageDialog(this, "Måste vara minst en positiv siffra", "Memory", 
												JOptionPane.ERROR_MESSAGE);
				changeTimer();
			} else {
				int newNbr = (int)(nbr * 1000);
				timer.setInitialDelay(newNbr);
			} 
		
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Skriv in en siffra", "Memory", 
											JOptionPane.ERROR_MESSAGE);
			changeTimer();
		}
	} // end of method changeTimer

	/*
	* Creates a new game by laying out new cards on an existing grid
	* in a cardPanel. All cards are laid out face down, and every card has exactly 
	* one copy on the board.
	* The cards are also given a mouse listener that gives them a slightly more shaded color when
	* they are selectable and the mouse hovers over them. See KeyListener methods further down.
	* @require (rows*cols)%2 == 0
	*/
	public void layOutNew (int rows, int cols) {
		this.removeAll(); //remove all laid out components from this panel.
		Kort[] playCards = new Kort[rows*cols];
		Verktyg.slumpOrdning(cards); //randomly arranges the elements in array cards.
		for (int i = 0; i < playCards.length/2; i++) {
			playCards[i] = cards[i];
			playCards[i].setStatus(Kort.Status.DOLT);
			playCards[i].addActionListener(this);
			playCards[i].addMouseListener(this);
			playCards[(rows*cols)/2 + i] = playCards[i].copy();
			playCards[(rows*cols)/2 + i].addActionListener(this);
			playCards[(rows*cols)/2 + i].addMouseListener(this);
		}
		Verktyg.slumpOrdning(playCards);	//scrambles the array that is to be 
											//laid out on the board.

		for (int i = 0; i < playCards.length; i++) {
			this.add(playCards[i]);
			///*DEBUG CODE! (print card's name in terminal:*/ 
			//System.out.println(i + " : " + 
			// ((ImageIcon)playCards[i].getMyIconConstant()).getDescription());
			/*End debug code*/
		}
		cardsLeft = playCards.length;
		this.validate();
	} //end of method layOutNew

	//LISTENER METHODS

	//ActionListener method
	@Override
	public void actionPerformed (ActionEvent e) {
		//If a card is clicked, and it is concealed, and if the delay between turns is over,
		//the card is flipped and the gameChecker method is invoked.
		if (e.getSource() instanceof Kort && !delay) {
			Kort source = (Kort)e.getSource();
			if (source.getStatus() == Kort.Status.DOLT) {
				flipUp(source);
				gameChecker(source);
			}
		}

		//If the timer has been started and fires an event, this method checks wheter the 
		//player has succeeded or failed, and calls the success method or failure method 
		//accordingly. The timer is then stopped and other things are allowed to happen in the 
		//game again.
		if (e.getSource() == timer) {
			if (success) {
				success();
				success = false;
			} else if (!success && turnCount == 0) {
				failure();
			}
			timer.stop();
			delay = false;
			//If a user wants to change the delay time by clicking 
			//menu Inställningar>>Ändra fördröjning, this is invoked
		} else if (e.getActionCommand() != null && e.getActionCommand().equals("setMyTimer")) {
			changeTimer();
		} 
	}

	//MousListener events

	//Gives card a sligthly darker blue hue if it is set to concealed (but not missing), and 
	//the mouse hovers over it.
	@Override
	public void mouseEntered (MouseEvent e) { 
		if (e.getSource() instanceof Kort) {
			Kort source = (Kort)e.getSource();
			if (source.getStatus() == Kort.Status.DOLT) {
				source.setRolloverBackground();
			}
		}
	}
	//Undos the effect of the mouseEntered method when the mouse no longer hovers over the card.
	@Override
	public void mouseExited (MouseEvent e) {
		if (e.getSource() instanceof Kort) {
			Kort source = (Kort)e.getSource();
			if (source.getStatus() == Kort.Status.DOLT) {
				source.setRegularBackground();
			}
		}
	}

	//To not uspet the sensitve MouseListener interface's feelings.
	@Override
	public void mouseClicked (MouseEvent foobar) {}
	@Override
	public void mousePressed (MouseEvent foobar) {}
	@Override
	public void mouseReleased (MouseEvent foobar) {}
} //End of class CardsPanel

/*
* The panel which displays the players and their scores. 
* A grid with x rows and one column is created, x being the number of players.
* If it's a single player game, two rows are created: one for the player and her score, and one
* for score history.
*/
class PlayersPanel extends JPanel {
	int rows;
	private final int WIDTH = 100;
	private SoloCounter soloCounter;
	
	public PlayersPanel(Player[] playerArray, int gridSize) {
		super();
		setPreferredSize(new Dimension(WIDTH, rows*gridSize));
		rows = playerArray.length;
		if (playerArray.length == 1) {
			setLayout(new GridLayout(2, 1));
			add(playerArray[0]);
			soloCounter = new SoloCounter();
			add(soloCounter);
		} else {
			setLayout(new GridLayout(rows, 1));
			soloCounter = null;
			for (int i = 0; i < rows; i++) {
				add(playerArray[i]);
			}
		}
	}

	//Pushes the score of a game to the panel with game history in a single player game.
	public void addSoloHistory(int newScore) {
		if (soloCounter != null) {
			soloCounter.addScore(newScore);
		}
	}
}//End on class PlayersPanel

/*
* Creates buttons for playing a new game or exiting the program. These are not needed when there
* is a menu handling these requests, but they may be added anyway.
*/
class ButtonsPanel extends JPanel {
	private JButton newGameButton;
	private JButton quitButton;

	public ButtonsPanel (ActionListener parent) {
		super(new FlowLayout());
		newGameButton = new JButton("Nytt spel");
		quitButton = new JButton("Avsluta");
		newGameButton.addActionListener(parent);
		quitButton.addActionListener(parent);
		newGameButton.setActionCommand("newGame");
		quitButton.setActionCommand("quit");
		add(newGameButton);
		add(quitButton);
	}
}//end of class ButtonsPanel

/*
* Menu bar for a memory game. Options include to start a new game, quit the program, change
* number of players, change number of rows and columns, and changing the time delay between plays.
*/
class GameMenu extends JMenuBar implements ActionListener {
	private JMenu game;
	private JMenu settings;
	private Memory parent;
	private CardsPanel cardsPanel;
	private JMenuItem newGame;
	private JMenuItem quit;
	private JMenuItem players;
	private JMenuItem myTimer;
	private JMenuItem changeDimension;

	public GameMenu (Memory parent, CardsPanel cardsPanel) {
		this.parent = parent;
		this.cardsPanel = cardsPanel;

		game = new JMenu("Spel");
		settings = new JMenu("Inställningar");
		
		//Configure "Spel"-menu with options "Nytt spel" and "Avsluta"

		//"Nytt spel"
		newGame = new JMenuItem("Nytt spel");
		newGame.setActionCommand("newGame");
		newGame.addActionListener(this);
		game.add(newGame);

		//"Avsluta"
		quit = new JMenuItem("Avsluta");
		quit.setActionCommand("quit");
		quit.addActionListener(this);
		game.add(quit);


		//Configure "Inställningar"-menu with option "Antal spelare", "Ändra fördröjning" and 
		//"Ändra rader och kolumner"

		//"Antal spelare"
		players = new JMenuItem("Antal spelare");
		players.setActionCommand("nbrOfPlayers");
		players.addActionListener(parent);
		settings.add(players);

		//"Ändra fördröjning"
		myTimer = new JMenuItem("Ändra fördröjning");
		myTimer.setActionCommand("setMyTimer");
		myTimer.addActionListener(cardsPanel);
		settings.add(myTimer);

		//"Ändra rader och kolumner"
		changeDimension = new JMenuItem("Ändra rader och kolumner");
		changeDimension.setActionCommand("changeDimension");
		changeDimension.addActionListener(parent);
		settings.add(changeDimension);

		//Add menus to menu bar
		this.add(game);
		this.add(settings);
	}

	/*
	* Responds to the menu items "Nytt spel", "Avsluta" and "Antal spelare"
	*/
	@Override
	public void actionPerformed (ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			JMenuItem item = (JMenuItem)e.getSource();
			if (item.getActionCommand().equals("newGame")) {
				parent.nyttSpel();	//starts new game
			} else if (item.getActionCommand().equals("quit")) {
				System.exit(0);	//Quit
			} else if (item.getActionCommand().equals("setNbrOfPlayers")) {
				parent.dispose();
				parent.main(new String[0]);	//disposes of this window and starts the program again.
			}
		}
	}
} //end of class GameMenu

/**
* The main game. The big wazooo. The fliest of the fly. The top component. 
* The only class with REAL class. The hub in the spokes. The spider in web.
* The "I" in "Team". Where the magic happens.
*/
public class Memory extends JFrame implements KeyListener, ActionListener{
	private final int GRID_SIZE = 100;
	private PlayersPanel playersPanel;
	private CardsPanel cardsPanel;
	private ButtonsPanel buttonsPanel;
	private Player[] playerArray;
	private int playerTurn;
	private int rows;
	private int cols;
	private GameMenu menuBar;

	//CONSTRUCTORS
	/**
	* Creates a new Memory game with given number of rows, columns and number of players.
	*/
	public Memory (int rows, int cols, int nbrOfPlayers) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(50,50);
		playerArray = createPlayerArray(nbrOfPlayers);
		
		addKeyListener(this);
		playerTurn = 0;
		//File successFile = new File("sound/success.wav");
		//File failureFile = new File("sound/failure.wav");
		this.rows = rows;
		this.cols = cols;
		//create seconadry level components
		JPanel mainPanel = new JPanel(new BorderLayout());
		playersPanel = new PlayersPanel(playerArray, GRID_SIZE);
		cardsPanel = new CardsPanel(rows, cols, GRID_SIZE, this); //give params rows and cols
		//Unnecessary thanks to menu /// buttonsPanel = new ButtonsPanel(this);
		setJMenuBar(new GameMenu(this, cardsPanel));
		add(playersPanel, BorderLayout.WEST);
		add(cardsPanel, BorderLayout.CENTER);
		//Unnecessary thanks to menu /// add(buttonsPanel, BorderLayout.SOUTH);
		pack();
		nyttSpel(rows, cols);
		setVisible(true);
	}

	//QUERIES
	/*
	* Creates a new array of new Players with the length given.
	*/
	private Player[] createPlayerArray (int nbrOfPlayers) {
		Player[] playerArray = new Player[nbrOfPlayers];
		if (nbrOfPlayers > 1) {
			for (int i = 0; i < nbrOfPlayers; i++) {
				playerArray[i] = new Player(i+1);
			}
		} else {
			playerArray[0] = new SoloPlayer();
		}
		return playerArray;
	}
	
	//COMMANDS
	/*
	* Starts a new game by resetting scores and turncounts and laying out a new gameboard with
	* the specified dimensions.
	*/
	private void nyttSpel(int rows, int cols) {
		cardsPanel.layOutNew(rows, cols);
		resetForNewGame();
	} // end method nyttSpel


	/* 
	* Overloaded method for new game which lays out a new board with the same number of 
	* rows and columns as the current board. Can be called from outside the class, 
	* but within the package
	*/
	void nyttSpel() {
		cardsPanel.layOutNew(this.rows, this.cols);
		resetForNewGame();
	}

	/*
	* Resets all values that need to be resetted before a new game.
	* - All players' scores are set to 0.
	* - Whoever has the turn gets unfocused, then player 1 gets focused.
	* - TurnCount in the cardsPanel is set to 0.
	* - Window is validated – all subcomponents are laid out again.
	*/
	private void resetForNewGame() {
		for (int i = 0; i < playerArray.length; i++) {
			playerArray[i].resetScore();
		}
		playerArray[playerTurn].setFocus(false);
		playerTurn = 0;
		playerArray[playerTurn].setFocus(true);
		cardsPanel.resetTurnCount();
		this.validate();
	}

	/*
	* Is invoked by a CardsPanel after a successful play. This simply increases the score of the 
	* player whose turn it was.
	*/
	void successfulPlay () {
		playerArray[playerTurn].incScore();
	}

	/*
	* Is invoked by a CardsPanel after a failed play. This simply gives the turn to the next player
	* in a multiplayer game. If it's a single player game, it increases the guss-count.
	*/
	void failedPlay () {
		if (playerArray.length == 1) {
			playerArray[0].incScore();
		} else {
			playerArray[playerTurn].setFocus(false);
			playerTurn = ++playerTurn % playerArray.length;
			playerArray[playerTurn].setFocus(true);
		}
	}

	/*
	* Invoked when no cards are left.
	* For a single player game, the guess-count of this game is added to history.
	* The user is then quieried if she wants to play again. If yes, a new game with the current
	* settings is started. If no, the program terminates.
	*/
	void gameOver () {
		if (playerArray.length == 1) {
			playersPanel.addSoloHistory(playerArray[0].getScore());
		}
		int choice = JOptionPane.showConfirmDialog(this, "Spela igen?", "Matchen är slut", 
													JOptionPane.	YES_NO_OPTION);
		if (choice == JOptionPane.YES_OPTION){
			nyttSpel();
		} 
		else {
			System.exit(0);
		}
	}

	/*
	* Opens a dialog where the user may change number of players in the game. A new game is 
	* then started with the given number of players. If the input is invalid (not a positive
	* integer), an error message is shown and the user is queried again.
	*/
	private void newNbrOfPlayers() {
		String nbrString = JOptionPane.showInputDialog(this, "Välj nytt antal spelare:", 
												"Memory", JOptionPane.QUESTION_MESSAGE);
		int nbr = -1;
		try {
			nbr = Integer.parseInt(nbrString);
			if (nbr < 1) {
				JOptionPane.showMessageDialog(this, "Måste vara minst en spelare", "Memory", 
												JOptionPane.ERROR_MESSAGE);
				newNbrOfPlayers();
			} else {
				playerArray = createPlayerArray(nbr);
				this.remove(playersPanel);
				playersPanel = new PlayersPanel(playerArray, GRID_SIZE);
				this.add(playersPanel, BorderLayout.WEST);
				nyttSpel();
				this.validate();
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Skriv in en siffra", "Memory", 
											JOptionPane.ERROR_MESSAGE);
			newNbrOfPlayers();
		}
	
	}

	//LISTENER METHODS

	//ActionListener method
	/**
	* If user chooses to change number of players, nbrOfPlayers method is invoked.
	* If user chooses to change number of rows and columns in the game, the window is disposed 
	* and the program restarts from the main function. The number of players used in the game 
	* is preserved by the number being passed to main, which uses this input when starting a 
	* new game.
	*/
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("nbrOfPlayers")) {
			newNbrOfPlayers();
		} 
		else if (e.getActionCommand().equals("changeDimension")) {
			this.dispose();
			String rememberNbrOfPlayers = Integer.toString(playerArray.length);
			String [] remember = {rememberNbrOfPlayers};
			main(remember);
		}
	}

	

	/**
	* DEBUG TOOL: Show all cards by pressing the L-key. 
	* (Doesn't work when one card is flipped).
	* ONLY FOR DEBUGGING!!!! No cheating （ゝ‿・）(´;︹・`)
	*/
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_L && cardsPanel.getTurnCount() == 0) {
			for (int i = 0; i < cardsPanel.getComponentCount(); i++) {
				if ( ((Kort)cardsPanel.getComponent(i) ).getStatus() == Kort.Status.DOLT) {
					((Kort)cardsPanel.getComponent(i)).setStatus(Kort.Status.SYNLIGT);
				}
			}
		}
	}

	public void keyReleased (KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_L && cardsPanel.getTurnCount() == 0) {
			for (int i = 0; i < cardsPanel.getComponentCount(); i++) {
				if ( ((Kort)cardsPanel.getComponent(i) ).getStatus() == Kort.Status.SYNLIGT) {
					((Kort)cardsPanel.getComponent(i)).setStatus(Kort.Status.DOLT);
				}
			}
		}
	}
	//FOOOBAR!
	public void keyTyped(KeyEvent foo) {} 
	
	//End of debug/cheat.


	/**
	* Asks user for input: how many rows and columns?
	* makes sure a number is put in as argument, otherwise it restarts.
	*
	* Checks: 
	* whether the number of rows and columns the user requested are
	* less than the maximum allowed number (limitation set by available 
	* number of icons).
	* Checks: 
	* whether number of requested squares is even. 
	* If conditions are not met, it restarts.
	* Else: If the number of rows and columns are legal, it creates a new game.
	*/
	public static void main (String[] args) {
		int rows = -1;
		int cols = -1;
		
		try {
			rows = Integer.parseInt(JOptionPane.showInputDialog(null, "Antal rader: ", 
			/*Do you love error handling*/					"Memory", JOptionPane.QUESTION_MESSAGE));
			cols = Integer.parseInt(JOptionPane.showInputDialog(null, "Antal kolumner: ", 
										"Memory", JOptionPane.QUESTION_MESSAGE));
		} catch (NumberFormatException n) {
			JOptionPane.showMessageDialog(null, "Du måste ange siffor.", 
											"Fel", JOptionPane.ERROR_MESSAGE);
			main(new String[0]);
		}

		File bildmapp = new File("bildmapp");
		File[] images = bildmapp.listFiles();
		if (images[0].getName().equals(".DS_Store")) {
			images = Arrays.copyOfRange(images, 1, images.length);
		}
		int cardsLength = images.length;
		//How 'bout dem conditionals?
		if (rows*cols < 2 || cols < 0 || rows < 0) {
			JOptionPane.showMessageDialog(null, "För få rutor. Du måste ha minst 2 rutor" 
											, "Fel antal rutor", JOptionPane.ERROR_MESSAGE);
			main(new String[0]);
		}
		else if (rows*cols > cardsLength*2) {
			JOptionPane.showMessageDialog(null, "För många rutor. Maximalt antal är " 
											+ cardsLength*2 + " rutor." , "Fel antal rutor", 
											JOptionPane.ERROR_MESSAGE);
			main(new String[0]);
		}
		else if ( (rows*cols) % 2 != 0 ) {
			JOptionPane.showMessageDialog(null, "Antalet rutor måste vara jämnt." , 
											"Fel antal rutor", JOptionPane.ERROR_MESSAGE);
			main(new String[0]);
		}
		else if (args == null || args.length == 0) {
			new Memory(rows, cols, 1);
		}
		else if (args.length == 1) {
			try {
				new Memory(rows, cols, Integer.parseInt(args[0]));
			} catch (NumberFormatException n) {
				System.exit(1);
			}
		}
	}
}