//Rikard Hjort & Kristoffer Knutsson
//Labbgrupp 14

import java.util.Random;

public class Verktyg {
	private static Random randGen = new Random();

	public static void slumpOrdning(Object[] arr) {
		//skapa en temporär array med samma längd som den givna.
		int length = arr.length;
		Object[] tempArr = new Object[length]; 

		for (int i = 0; i < length; i++) {
			int rand = randGen.nextInt(length);
			
			while (tempArr[rand] != null) {
				rand = randGen.nextInt(length);
			}
			tempArr[rand] = arr[i];
		}

		for (int i = 0; i < length; i++) {
			arr[i] = tempArr[i];
		}
	}
}

