package counter;

// Kristoffer Knuttson & Rikard Hjort
// Labbgrupp 4

public class CounterModel implements CounterInterface {

	private int count;
	private final int MODULO;
	private static int nbrOfCounters = 0;

// CONSTRUCTORS

	/**
	* Class constructor without parameters.
	* @ensure 	count == 0
	*			for all counts, 10 > count && count > 0
	*			Number of counters += 1
	*/
	public CounterModel () {
		this.count = 0;
		this.MODULO = 10;
		setCounters();
	}

	/**
	* Class constructor with number limit.
	* The modulo number is the number of integers that the counter can represent. The largest 
	* number that a counter can represent is modulo - 1. The smallest is 0.
	* @param int modulo.
	* @require modulo >= 0
	* @ensure 	count == 0
	*			for all counts, modulo > count && count > 0
	*			Number of counters += 1
	*/
	public CounterModel (int modulo) {
		if (modulo <= 0)
			throw new IllegalArgumentException("Can't accept negative parameters");
		this.count = 0;
		this.MODULO = modulo;
		setCounters();
	}


//QUERIES
	/**
	* Returns the counter's current count.
	* @ensure result  >= 0 && result < getModulo()
	* @return current count.
	*/
	public int getValue() {
		return this.count;
	} 
	
	/**
	* Returns the top value of the counter + 1.
	* @ensure if |count| > modulo then counter.getValue() returns count % modulo
	* @return max count + 1 
	*/
	public int getModulo () {
		return this.MODULO;
	}

	/**
	* Returns the total number of counters created by this class.
	* @ensure result >= 0
	* @return number of counters created. 
	*/
	public static int getNbrOfCounters () {
		return nbrOfCounters;
	}

	/**
	* Returns a string representation of the counter.
	* The returned string is on the form "count = " + count + ", modulo =" modulo; 
	*/
	@Override
	public String toString () {
		return "count = " + Integer.toString(this.count) + ", modulo = " + 
		Integer.toString(this.MODULO);
	}

	@Override
	public boolean equals (Object obj) {
		if (obj == null || obj.getClass() != this.getClass())
			return false;
		else {
			CounterModel tempObj = (CounterModel)obj;
			return ( tempObj.getValue() == this.getValue() && 
					tempObj.getModulo() == this.getModulo() );
		}
	}

//COMMANDS

	/**
	*Increases the count by 1.
	*
	*/
	public void increment () {
		this.count = (this.count + 1) % MODULO;	
	} 

	/**
	*Decreases the count by 1.
	*/	
	public void decrement () {
		if (this.count > 0)
			this.count--;
		else
			this.count = MODULO - 1;
	} 

	/**
	* Sets the count to 0.
	*/
	public void reset () {
		this.count = 0;
	}

	private static void setCounters() {
		nbrOfCounters++;
	}

} // end class CounterModel