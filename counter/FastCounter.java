package counter;

// Rikard Hjort & Kristoffer Knutsson 
// Labbgrupp 14 

public class FastCounter extends CounterModel {

	private int step;

//CONSTRUCTOR
	/**
	* Class counstructor without parameters.
	* @ensure 	count == 0
	*			for all counts, 15 > count && count >= 0
	*			Number of counters += 1
	*/
	public FastCounter() {
	super(15);
	this.step = 5;
	}

	/**
	* Class counstructor with number limit and step size.
	* @ensure 	count == 0
	*			for all counts, modulo > count && count >= 0 
	*			number of counter += 1
	*/
	public FastCounter (int modulo, int step) {
		super(modulo);
		if (step > 0) {
			this.step = step;
		} else {
			throw new IllegalArgumentException("Step size must be larger than 0.");
		}
	}

//QUERIES
	/**
	* Returns the int value which upMany() and downMany() increases or decreases the count.
	* @return step size
	*/
	public int getStep() {
		return this.step;
	}

	@Override
	public String toString() {
		return super.toString() + ", step = " + this.step;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != this.getClass())
			return false;
		else {
			FastCounter tempObj = (FastCounter)obj;
			return ( tempObj.getValue() == this.getValue() && tempObj.getModulo() == 
				this.getModulo() && tempObj.getStep() == this.getStep() );
		}
	}

//COMMANDS
	/**
	* Increases count by the set number of steps.
	* @ensure new count = old count + getStep() ) % getModulo()
	*/
	public void upMany() {
		for (int i = 0; i < this.step; i++)
		this.increment();	
	}

	/**
	* Decreases count by the set number of steps.
	* @ensure new count = old count - getStep() ) % getModulo()
	*/
	public void downMany() {
		for (int i = 0; i < this.step; i++)
		this.decrement();	
	}

} // end FastCounter