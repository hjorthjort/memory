package ratnum;

/* Kristoffer Knutsson & Rikard Hjort
 * Grupp 14
 */

/**
 * A class representing a rational number 
 * The class RatNum creates objects that are rational number on the form a/b, 
 * and defines a few useful methods for rational numbers.
 */

public class RatNum {
	
	//PRIVATE COMPONENTS
	
	private final int numerator;
	private final int denominator;
	
	//CONSTRUCTORS
	
	/**
	 * Constructor without parameters.
	 * When no parameters are given the RatNum object is constructed as 0/1.
	 */
	public RatNum () {
		this.numerator = 0;
		this.denominator = 1;
	}
	
	/**
	 * Constructor with one int parameter.
	 * When only one integer is given as parameter the RatNum object is constructed as numerator/1. 
	 */
	public RatNum (int numerator) {
		this.numerator = numerator;
		this.denominator = 1;
	}
	
	/**
	 * Constructor with two int parameters.
	 * When two integers are given as parameters the RatNum object is constructed as numerator/denominator,
	 * written on the simplest form. If numerator and denominator are negative, both are switched to positive. 
	 * If only the denominator is negative, the minus sign is assigned to the numerator instead.
	 */
	public RatNum (int numerator, int denominator) {
		
		if (denominator == 0) 
			throw new NumberFormatException("Denominator = 0");
		else {
			int tempNumerator;
			int tempDenominator;
			
			// Change the fractions to its simplest form.
			if (gcd(numerator, denominator) != 1) {
				tempNumerator = numerator/gcd(numerator, denominator);
				tempDenominator = denominator/gcd(numerator,denominator);
			} else {
				tempNumerator = numerator;
				tempDenominator = denominator;
			}
				
			// Make the denominator is positive, without changing the value of the rational number.
			if ( (tempNumerator < 0) && (tempDenominator < 0) ) {
				this.numerator = absoluteValue(tempNumerator);
				this.denominator = absoluteValue(tempDenominator);
			} else if (denominator < 0) {
				this.numerator = tempNumerator * -1;
				this.denominator = absoluteValue(tempDenominator);
			} else {
				this.numerator = tempNumerator;
				this.denominator = tempDenominator;
			}
		}
	}
	
	/**
	 * Constructor with a RatNum as parameter.
	 * When a RatNum object is given as a parameter, the new RatNum object is constructed with the same 
	 * instance variables.
	 */
	public RatNum (RatNum r) {
		this.numerator = r.getNumerator();
		this.denominator = r.getDenominator();	
	}
	
	/**
	 * Constructor with a String parameter.
	 * When a string is given as a parameter, the new RatNum object is constructed by using the parse function in 
	 * this class.
	 * If the string is written as a rational number on the form "numerator/denominator", or just "numerator" a new
	 * RatNum object is created. Numerator or denominator may be negative, indicated with a minus sign at the beginning
	 * of the number. Both numerator and denominator must be integers.
	 */
	public RatNum (String string) {
		this(parse(string));
	}
	
	//end of constructors
	
	//QUERIES
	/**
	 * Returns the value of the numerator.
	 */
	public int getNumerator () {
		return numerator;
	}
	
	/**
	 * Returns the value of the denominator.
	 */
	public int getDenominator () {
		return denominator;
	}
	
	/**
	 * Returns the rational number object in the form of a string "numerator/denominator".
	 */
	public String toString () {
		int whole = numerator / denominator;
		int fraction = numerator % denominator;
		String s = "";
		if (whole != 0) {
			s = Integer.toString(whole);
		} if (fraction != 0) {
			fraction = absoluteValue(fraction);
			s += " " + fraction + "/" + denominator;
		}
		return s;
	}
	
	/**
	 * Returns an approximation of the rational number in the format of a double.
	 */
	public double toDouble () {
		return (double)numerator/denominator;
	}
	
	
	public boolean equals (Object obj) {
		
		if( obj == null || obj.getClass() != this.getClass() )
			return false;
			else {
			RatNum newRatnum = (RatNum)obj;
			
			/* Below we take advantage of the fact that all RatNum objects always are in their simplest forms, with
			 * no common divisors in the numerator and denominator except 1. Since they are written on their simplest forms,
			 * we can safely assume that both the numerator and denominator of both the RatNum objects must be equal for
			 * them to be equal.
			 */
			return (numerator == newRatnum.getNumerator() && denominator == newRatnum.getDenominator());
		}
	}
	
	/**
	 * Returns if the given parameter represents a smaller number than the object.
	 */
	public boolean lessThan (RatNum r) {
		/*Here we will use the fact that (a/b)/(c/d) = ad/bc, and that if a/b >= c/d, then (a/b)/(c/d) >=1 
		and thus ad/bc >= 1.
		*/
		int compareObject = numerator * r.getDenominator();
		int compareArgument = denominator * r.getNumerator();
		
		return compareObject < compareArgument ? true : false;
	}
	
	/**
	 * Returns a new RatNum object which is the sum of the invoked object and the parameter.
	 * BEWARE OF OVERFLOW
	 * Remember that for a/b + c/d the sum ad + bc and the product bc must not be larger than 
	 * the allowed values for an integer.
	 */
	public RatNum add(RatNum r) {
		int newNumerator;
		int newDenominator;
		
		newNumerator = numerator*r.getDenominator() + r.getNumerator()*denominator;
		newDenominator = denominator*r.getDenominator();
		
		return new RatNum(newNumerator, newDenominator);
	}
	
	/**
	 * Returns a new RatNum object which is the difference of the invoked object and the parameter.
	 * BEWARE OF OVERFLOW! 
	 * Remember that for a/b - c/d the difference ad - bc and the product bc must not be larger than 
	 * the allowed values for an integer.
	 */
	public RatNum sub(RatNum r) {
		int newNumerator;
		int newDenominator;
		
		newNumerator = numerator*r.getDenominator() - r.getNumerator()*denominator;
		newDenominator = denominator*r.getDenominator();
		
		return new RatNum(newNumerator, newDenominator);
	}
	
	/**
	 * Returns the product of the object and the parameter. 
	 * BEWARE OF OVERFLOW!
	 * The product of the numerators or the product of the denominators may not be larger than the 
	 * allowed values for an integer.
	 */
	public RatNum mul(RatNum r) {
		return new RatNum(numerator*r.getNumerator(), denominator*r.getDenominator());
	}
	
	/**
	 * Returns the quotient of the object and the parameter as a new RatNum object.
	 */
	public RatNum div(RatNum r) {
		return new RatNum(numerator*r.getDenominator(), denominator*r.getNumerator());
	}
	
	//end of queries.
	
	//CLASS METHODS
	
	/**
	 * Returns the greatest common divisor of a and b.
	 */
	public static int gcd (int a, int b) { 	//I think this is a strange method to have in the class RatNum. 
											//It seems to belong in it's own class.
		a = absoluteValue(a);
		b = absoluteValue(b);
				
		if ( (a == 0) && (b == 0) )
			throw new IllegalArgumentException();
		else if (b == 0)
			return a;
		else if (a == 0)
			return b;
		else {
			//Euclide's algorithm
			int r; 
			while (a % b != 0) {
				r = a % b;
				a = b;
				b = r;
			}
			return b;
		}
	}
	
	/**
	 * Parses a string to see if it describes a rational number. Allowed formats are "numerator/denominator" and 
	 * "numerator". Either numerator or denominator can be negative, indicated by a '-'. 
	 * This method creates a new RatNum object and returns a reference to it.
	 */
	public static RatNum parse (String string) {
	
		/* Finds the /-character in the given string. If no /-character is found, one is added at the end
		 * together with a 1 ("/1").
		 * If the /-character is the last in the string, a "1" is added to the end.
		 * (Note: If string contains any other invalid characters, this error will be caught later by 
		 * the Integer.parseInt-method.*/
		int slashPosition = string.indexOf('/');
		if (slashPosition == -1) {
			string += "/1";
			slashPosition = string.indexOf('/');
		} else if (slashPosition == string.length()-1) // This makes strings on the form "a/" into valid RatNum objects
			string += "1";								// a/1.
			
		/*Splits string into two new strings, one made up of all the characters to the left of the /,
		 * and one made up of all the characters to the right of the /
		 */
		String numeratorString = string.substring(0, slashPosition);
		String denominatorString = string.substring(slashPosition+1);
	
		/* Parses the two new strings and converts them integers, if possible, and stores them in two variables, 
		 * numerator and denominator. If the string contains anything but decimal digits or a minus sign in 
		 * the first position, the function parseInt throws NumberFormatException.
		 */
		 int numerator = Integer.parseInt(numeratorString);
		 int denominator = Integer.parseInt(denominatorString);
			
		/*Creates new RatNum object with the parsed numerators and denominators
		 */
		return new RatNum(numerator, denominator);
		
	}
	
	//PRIVATE ClASS METHODS
	
	/*Calculates the absolute value of an int, num.
	 */
	private static int absoluteValue (int num) {
		return num < 0 ? num * -1 : num;
	} //end of class methods.
} //end of class.